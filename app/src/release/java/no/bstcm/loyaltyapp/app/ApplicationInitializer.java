package no.bstcm.loyaltyapp.app;

import android.content.Context;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;
import no.bstcm.loyaltyapp.app.App;
import no.bstcm.loyaltyapp.app.dagger.components.DaggerApplicationComponent;
import no.bstcm.loyaltyapp.app.dagger.modules.ApplicationModule;
import no.bstcm.loyaltyapp.components.common.ui.ScreenOrientationUtils;
import okhttp3.OkHttpClient;
import timber.log.Timber;

public class ApplicationInitializer {
    public static void init(App app) {
        Fabric.with(app.getApplicationContext(), new Crashlytics());
        Timber.plant(new CrashlyticsLogTree());

        ScreenOrientationUtils.forcePortrait(app);

        app.setComponent(DaggerApplicationComponent
                                 .builder()
                                 .applicationModule(new ApplicationModule(app, buildHttpClient()))
                                 .build());
    }

    private static OkHttpClient buildHttpClient() {
        return new OkHttpClient();
    }
}
