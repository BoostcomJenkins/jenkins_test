package no.bstcm.loyaltyapp.app;

import com.facebook.stetho.Stetho;
import com.facebook.stetho.okhttp3.StethoInterceptor;

import no.bstcm.loyaltyapp.app.dagger.components.DaggerApplicationComponent;
import no.bstcm.loyaltyapp.app.dagger.modules.ApplicationModule;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import timber.log.Timber;

public class ApplicationInitializer {
    public static void init(App app) {
        Timber.plant(new Timber.DebugTree());

        Stetho.initializeWithDefaults(app);

        app.setComponent(DaggerApplicationComponent
                                 .builder()
                                 .applicationModule(new ApplicationModule(app, buildHttpClient()))
                                 .build());
    }

    private static OkHttpClient buildHttpClient() {
        return new OkHttpClient()
                .newBuilder()
                .addNetworkInterceptor(new StethoInterceptor())
                .addInterceptor(buildLoggingInterceptor())
                .build();
    }

    private static HttpLoggingInterceptor buildLoggingInterceptor() {
        HttpLoggingInterceptor.Logger logger = (message) -> Timber
                .tag("OkHttp")
                .d(message);
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(logger);
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);

        return loggingInterceptor;
    }
}
