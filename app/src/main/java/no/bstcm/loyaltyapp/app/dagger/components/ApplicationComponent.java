package no.bstcm.loyaltyapp.app.dagger.components;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import no.bstcm.loyaltyapp.app.Analytics;
import no.bstcm.loyaltyapp.app.App;
import no.bstcm.loyaltyapp.app.NavigationList;
import no.bstcm.loyaltyapp.app.dagger.modules.AppLinkingModule;
import no.bstcm.loyaltyapp.app.dagger.modules.ApplicationModule;
import no.bstcm.loyaltyapp.app.dagger.modules.ArticlesModule;
import no.bstcm.loyaltyapp.app.dagger.modules.BeaconsModule;
import no.bstcm.loyaltyapp.app.dagger.modules.CouponsModule;
import no.bstcm.loyaltyapp.app.dagger.modules.DmpModule;
import no.bstcm.loyaltyapp.app.dagger.modules.IdentityModule;
import no.bstcm.loyaltyapp.app.dagger.modules.PusherModule;
import no.bstcm.loyaltyapp.app.dagger.modules.ShopsModule;
import no.bstcm.loyaltyapp.app.dagger.modules.WebModule;
import no.bstcm.loyaltyapp.app.dagger.modules.WelcomeModule;
import no.bstcm.loyaltyapp.components.identity.PostAuthenticationOperation;
import no.bstcm.loyaltyapp.components.identity.core.AuthenticationNavigatorFactory;
import no.bstcm.loyaltyapp.components.identity.core.SessionProvider;
import okhttp3.OkHttpClient;

@Singleton
@Component(
        modules = {
                ApplicationModule.class,
                ArticlesModule.class,
                CouponsModule.class,
                IdentityModule.class,
                ShopsModule.class,
                WelcomeModule.class,
                PusherModule.class,
                DmpModule.class,
                AppLinkingModule.class,
                WebModule.class,
                BeaconsModule.class
        })
public interface ApplicationComponent {
    App getApplication();

    Context getApplicationContext();

    NavigationList getNavigationList();

    OkHttpClient getOkHttpClient();

    Analytics getAnalytics();

    SessionProvider getSessionProvider();

    AuthenticationNavigatorFactory getAuthenticationNavigatorFactory();

    PostAuthenticationOperation getPostAuthenticationOperation();

    no.bstcm.loyaltyapp.components.articles.Config getArticlesConfig();

    no.bstcm.loyaltyapp.components.coupons.Config getCouponsConfig();

    no.bstcm.loyaltyapp.components.identity.Config getIdentityConfig();

    no.bstcm.loyaltyapp.components.shops.Config getShopsConfig();

    no.bstcm.loyaltyapp.components.welcome.Config getWelcomeConfig();

    no.bstcm.loyaltyapp.components.pusher.Config getPusherConfig();

    no.bstcm.loyaltyapp.components.app_linking.Config getAppLinkingConfig();

    no.bstcm.loyaltyapp.components.web.Config getWebConfig();

    no.bstcm.loyaltyapp.components.beacons.Config getBeaconsConfig();
}
