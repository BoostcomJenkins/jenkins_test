package no.bstcm.loyaltyapp.app;

import no.bstcm.loyaltyapp.components.identity.PostAuthenticationOperation;
import no.bstcm.loyaltyapp.components.identity.core.UserSession;
import no.bstcm.loyaltyapp.components.pusher.Pusher;

public class PostAuthenticationOperationImpl implements PostAuthenticationOperation {
    private final NavigationList navigationList;

    public PostAuthenticationOperationImpl(NavigationList navigationList) {
        this.navigationList = navigationList;
    }

    @Override
    public void execute(UserSession session) {
        navigationList.setSessionScope();
        Pusher.startReceivingNotifications(session.getMsisdn().format());
    }
}
