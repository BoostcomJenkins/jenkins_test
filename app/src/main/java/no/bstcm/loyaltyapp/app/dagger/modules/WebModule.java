package no.bstcm.loyaltyapp.app.dagger.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import no.bstcm.loyaltyapp.components.analytics.dmp.WebAnalyticsDmp;
import no.bstcm.loyaltyapp.components.common.ui.navigation.MainNavigationActivityDelegateFactory;
import no.bstcm.loyaltyapp.components.dmp.tracker.Tracker;
import no.bstcm.loyaltyapp.components.web.Config;

@Module
public class WebModule {
    @Provides
    @Singleton
    Config provideConfig(Tracker tracker, MainNavigationActivityDelegateFactory mainNavigationActivityDelegateFactory) {
        return Config.builder()
                .setMainNavigationActivityDelegateFactory(mainNavigationActivityDelegateFactory)
                .setAnalytics(new WebAnalyticsDmp(tracker))
                .build();
    }
}
