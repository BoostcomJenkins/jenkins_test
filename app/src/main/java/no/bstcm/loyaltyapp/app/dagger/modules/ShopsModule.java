package no.bstcm.loyaltyapp.app.dagger.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import no.bstcm.loyaltyapp.app.Config;
import no.bstcm.loyaltyapp.components.analytics.dmp.ShopsAnalyticsDmp;
import no.bstcm.loyaltyapp.components.common.ui.navigation.MainNavigationActivityDelegateFactory;
import no.bstcm.loyaltyapp.components.dmp.tracker.Tracker;
import okhttp3.OkHttpClient;

@Module
public class ShopsModule {
    @Provides
    @Singleton
    no.bstcm.loyaltyapp.components.shops.Config provideConfig(Tracker tracker,
                                                              MainNavigationActivityDelegateFactory navigationDelegateFactory,
                                                              OkHttpClient okHttpClient) {
        return no.bstcm.loyaltyapp.components.shops.Config.builder()
                .setApiBaseUrl(Config.API_SHOPS_BASE_URL)
                .setApiKey(Config.API_SHOPS_KEY)
                .setDataProvider(no.bstcm.loyaltyapp.components.shops.Config.DataProvider.ScraperApi)
                .setAnalytics(new ShopsAnalyticsDmp(tracker))
                .setMainNavigationActivityDelegateFactory(navigationDelegateFactory)
                .setOkHttpClient(okHttpClient)
                .build();
    }
}
