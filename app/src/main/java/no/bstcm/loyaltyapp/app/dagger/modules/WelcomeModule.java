package no.bstcm.loyaltyapp.app.dagger.modules;

import java.util.Arrays;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import no.bstcm.loyaltyapp.oslo_s.R;
import no.bstcm.loyaltyapp.components.analytics.dmp.WelcomeAnalyticsDmp;
import no.bstcm.loyaltyapp.components.dmp.tracker.Tracker;
import no.bstcm.loyaltyapp.components.welcome.Config;
import no.bstcm.loyaltyapp.components.welcome.PageModel;

@Module
public class WelcomeModule {
    @Provides
    @Singleton
    Config provideConfig(Tracker tracker) {
        return Config
                .builder()
                .setPages(Arrays.asList(
                        new PageModel(R.drawable.welcome_page_1, R.string.welcome_page_1),
                        new PageModel(R.drawable.welcome_page_2, R.string.welcome_page_2),
                        new PageModel(R.drawable.welcome_page_3, R.string.welcome_page_3)))
                .setAnalytics(new WelcomeAnalyticsDmp(tracker))
                .build();
    }
}
