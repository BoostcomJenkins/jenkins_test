package no.bstcm.loyaltyapp.app.dagger.modules;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import no.bstcm.loyaltyapp.app.Analytics;
import no.bstcm.loyaltyapp.app.App;
import no.bstcm.loyaltyapp.app.NavigationList;
import no.bstcm.loyaltyapp.app.dagger.components.DaggerMainNavigationActivityComponent;
import no.bstcm.loyaltyapp.app.dagger.components.MainNavigationActivityComponent;
import no.bstcm.loyaltyapp.components.app_linking.core.ApplicationOpenListener;
import no.bstcm.loyaltyapp.components.common.ui.navigation.MainNavigationActivityDelegateFactory;
import no.bstcm.loyaltyapp.components.dmp.tracker.Config;
import no.bstcm.loyaltyapp.components.dmp.tracker.DmpTracker;
import no.bstcm.loyaltyapp.components.dmp.tracker.Tracker;
import no.bstcm.loyaltyapp.components.identity.core.SessionProvider;
import no.bstcm.loyaltyapp.components.welcome.WelcomeStatePersistence;
import okhttp3.OkHttpClient;

@Module
public class ApplicationModule {
    private final App application;
    private final OkHttpClient okHttpClient;

    public ApplicationModule(App app, OkHttpClient okHttpClient) {
        application = app;
        this.okHttpClient = okHttpClient;
    }

    @Provides
    @Singleton
    App provideApplication() {
        return application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return application;
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient() {
        return okHttpClient;
    }

    @Provides
    @Singleton
    Tracker provideTracker(Context context, Config config) {
        return DmpTracker.getInstance(context,config);
    }

    @Provides
    @Singleton
    Analytics provideAnalytics(Tracker tracker) {
        return new Analytics(tracker);
    }

    @Provides
    @Singleton
    NavigationList provideNavigationList(SessionProvider sessionProvider) {
        return new NavigationList(sessionProvider);
    }

    @Provides
    @Singleton
    MainNavigationActivityDelegateFactory provideMainNavigationActivityDelegateFactory() {
        return (activity) -> {
            MainNavigationActivityComponent navigationComponent = DaggerMainNavigationActivityComponent
                    .builder()
                    .applicationComponent(((App) activity.getApplication()).getComponent())
                    .activityModule(new ActivityModule(activity))
                    .build();

            return navigationComponent.getActivityDelegate();
        };
    }

    @Provides
    @Singleton
    ApplicationOpenListener provideApplicationOpenListener(App application) {
        return intent -> WelcomeStatePersistence.doNotShowWelcomeScreen(application);
    }
}
