package no.bstcm.loyaltyapp.app;

import no.bstcm.loyaltyapp.components.beacons.Beacons;
import no.bstcm.loyaltyapp.components.identity.PostLogoutOperation;
import no.bstcm.loyaltyapp.components.pusher.Pusher;

public class PostLogoutOperationImpl implements PostLogoutOperation {
    private final NavigationList navigationList;

    public PostLogoutOperationImpl(NavigationList navigationList) {
        this.navigationList = navigationList;
    }

    @Override
    public void execute() {
        navigationList.setNullSessionScope();
        Pusher.stopReceivingNotifications();
        Beacons.clearSession();
    }
}
