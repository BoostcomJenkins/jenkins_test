package no.bstcm.loyaltyapp.app.dagger.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import no.bstcm.loyaltyapp.app.Config;
import no.bstcm.loyaltyapp.components.analytics.dmp.CouponsAnalyticsDmp;
import no.bstcm.loyaltyapp.components.common.ui.navigation.MainNavigationActivityDelegateFactory;
import no.bstcm.loyaltyapp.components.dmp.tracker.Tracker;
import no.bstcm.loyaltyapp.components.identity.core.AuthenticationNavigatorFactory;
import no.bstcm.loyaltyapp.components.identity.core.SessionProvider;
import okhttp3.OkHttpClient;

@Module
public class CouponsModule {
    @Provides
    @Singleton
    no.bstcm.loyaltyapp.components.coupons.Config provideConfig(Tracker tracker,
                                                                SessionProvider sessionProvider,
                                                                MainNavigationActivityDelegateFactory navigationDelegateFactory,
                                                                AuthenticationNavigatorFactory authenticationNavigatorFactory,
                                                                OkHttpClient okHttpClient) {
        return no.bstcm.loyaltyapp.components.coupons.Config.builder()
                .setApiBaseUrl(Config.API_COUPONS_BASE_URL)
                .setApiKey(Config.API_COUPONS_KEY)
                .setActivationTimeInSeconds(30)
                .setUiDateFormat("dd/MM/yyyy")
                .setAnalytics(new CouponsAnalyticsDmp(tracker))
                .setSessionProvider(sessionProvider)
                .setMainNavigationActivityDelegateFactory(navigationDelegateFactory)
                .setAuthenticationNavigatorFactory(authenticationNavigatorFactory)
                .setOkHttpClient(okHttpClient)
                .build();
    }
}
