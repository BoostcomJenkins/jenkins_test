package no.bstcm.loyaltyapp.app.dagger;

public interface HasComponent<T> {
    T getComponent();
}
