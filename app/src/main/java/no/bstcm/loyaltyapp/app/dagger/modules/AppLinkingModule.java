package no.bstcm.loyaltyapp.app.dagger.modules;

import android.content.Context;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import no.bstcm.loyaltyapp.app.OpeningHoursWebViewActivity;
import no.bstcm.loyaltyapp.app.WebActivity;
import no.bstcm.loyaltyapp.components.analytics.core.AppLinkingAnalytics;
import no.bstcm.loyaltyapp.components.analytics.dmp.AppLinkingAnalyticsDmp;
import no.bstcm.loyaltyapp.components.app_linking.Config;
import no.bstcm.loyaltyapp.components.app_linking.core.AppLinkHandler;
import no.bstcm.loyaltyapp.components.app_linking.core.ApplicationOpenListener;
import no.bstcm.loyaltyapp.components.app_linking.core.UriValidator;
import no.bstcm.loyaltyapp.components.app_linking.handler.LauncherAppLinkHandler;
import no.bstcm.loyaltyapp.components.app_linking.handler.TopLevelLinkHandler;
import no.bstcm.loyaltyapp.components.app_linking.processor.AppLinkHandlerProcessor;
import no.bstcm.loyaltyapp.components.app_linking.validator.ManifestUriValidator;
import no.bstcm.loyaltyapp.components.articles.categories.ArticleCategoriesActivity;
import no.bstcm.loyaltyapp.components.coupons.categories.CouponCategoriesActivity;
import no.bstcm.loyaltyapp.components.dmp.tracker.Tracker;
import no.bstcm.loyaltyapp.components.identity.member_card.MemberCardActivity;
import no.bstcm.loyaltyapp.components.identity.profile.ProfileActivity;
import no.bstcm.loyaltyapp.components.opening_hours.shopping_mall.OpeningHoursActivity;
import no.bstcm.loyaltyapp.components.shops.ShopCollectionActivity;

@Module
public class AppLinkingModule {

    @Provides
    @Singleton
    UriValidator provideUriValidator(Context context) {
        return new ManifestUriValidator(context);
    }

    @Provides
    @Singleton
    AppLinkHandler provideAppLinkHandler(Context context, UriValidator validator) {
        return new AppLinkHandlerProcessor(
                Arrays.asList(
                        new TopLevelLinkHandler("art", ArticleCategoriesActivity.class, context),
                        new TopLevelLinkHandler("cpn", CouponCategoriesActivity.class, context),
                        new TopLevelLinkHandler("hpa", WebActivity.class, context),
                        new TopLevelLinkHandler("hrs", OpeningHoursWebViewActivity.class, context),
                        new TopLevelLinkHandler("mbc", MemberCardActivity.class, context),
                        new TopLevelLinkHandler("prl", ProfileActivity.class, context),
                        new TopLevelLinkHandler("shp", ShopCollectionActivity.class, context),
                        new LauncherAppLinkHandler(context)
                ), validator);
    }

    @Provides
    @Singleton
    AppLinkingAnalytics provideAppLinkAnalytics(Tracker tracker) {
        return new AppLinkingAnalyticsDmp(tracker);
    }

    @Provides
    @Singleton
    Config provideAppLinkingConfig(AppLinkingAnalytics analytics, AppLinkHandler appLinkHandler, ApplicationOpenListener applicationOpenListener) {
        return Config
                .builder()
                .setHandler(appLinkHandler)
                .setAnalytics(analytics)
                .setApplicationOpenListener(applicationOpenListener)
                .build();
    }

}
