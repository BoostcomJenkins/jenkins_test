package no.bstcm.loyaltyapp.app;

import java.util.ArrayList;
import java.util.List;

import no.bstcm.loyaltyapp.components.articles.categories.ArticleCategoriesActivity;
import no.bstcm.loyaltyapp.components.beacons.BeaconsSettingsActivity;
import no.bstcm.loyaltyapp.oslo_s.R;
import no.bstcm.loyaltyapp.components.common.ui.navigation.MainNavigation;
import no.bstcm.loyaltyapp.components.common.ui.navigation.NavigationAction;
import no.bstcm.loyaltyapp.components.common.ui.navigation.NavigationActionFactory;
import no.bstcm.loyaltyapp.components.common.ui.navigation.ScopedNavigationActionProvider;
import no.bstcm.loyaltyapp.components.coupons.categories.CouponCategoriesActivity;
import no.bstcm.loyaltyapp.components.identity.core.SessionProvider;
import no.bstcm.loyaltyapp.components.identity.login.LoginActivity;
import no.bstcm.loyaltyapp.components.identity.member_card.MemberCardActivity;
import no.bstcm.loyaltyapp.components.identity.profile.ProfileActivity;
import no.bstcm.loyaltyapp.components.opening_hours.shopping_mall.OpeningHoursActivity;

public class NavigationList implements MainNavigation.ActionListProvider {

    private static final int UNSCOPED = ScopedNavigationActionProvider.Scope.UNSCOPED;
    private static final int SESSION_SCOPE = 2;
    private static final int NULL_SESSION_SCOPE = 4;

    private static ArrayList<NavigationAction> items = new ArrayList<>();
    private static NavigationActionFactory factory = new NavigationActionFactory();

    static {
        items.add(factory
                          .startActivity(CouponCategoriesActivity.class)
                          .labelText(R.string.main_navigation_coupons)
                          .labelIcon(R.drawable.ic_coupons)
                          .keepSingleInstance()
                          .build());

        items.add(factory
                          .startActivity(ArticleCategoriesActivity.class)
                          .labelText(R.string.main_navigation_articles)
                          .labelIcon(R.drawable.ic_articles)
                          .keepSingleInstance()
                          .build());

        items.add(factory
                          .startActivity(OpeningHoursWebViewActivity.class)
                          .labelText(R.string.main_navigation_opening_hours)
                          .labelIcon(R.drawable.ic_opening_hours)
                          .keepSingleInstance()
                          .build());

        items.add(factory
                          .startActivity(no.bstcm.loyaltyapp.components.shops.ShopCollectionActivity.class)
                          .labelText(R.string.main_navigation_shops)
                          .labelIcon(R.drawable.ic_shops)
                          .keepSingleInstance()
                          .build());

        items.add(factory
                          .startActivity(WebActivity.class)
                          .labelText(R.string.main_navigation_website)
                          .labelIcon(R.drawable.ic_web)
                          .keepSingleInstance()
                          .build());

        items.add(factory
                          .startActivity(MemberCardActivity.class)
                          .labelText(R.string.main_navigation_member_card)
                          .labelIcon(R.drawable.ic_member_card)
                          .keepSingleInstance()
                          .build());

        items.add(factory
                          .startActivityForResult(LoginActivity.class, LoginActivity.REQUEST_AUTHENTICATION)
                          .labelText(R.string.main_navigation_login)
                          .labelIcon(R.drawable.ic_profile)
                          .setScope(NULL_SESSION_SCOPE)
                          .build());

        items.add(factory
                          .startActivity(ProfileActivity.class)
                          .labelText(R.string.main_navigation_profile)
                          .labelIcon(R.drawable.ic_profile)
                          .keepSingleInstance()
                          .requireAuthentication()
                          .setScope(SESSION_SCOPE)
                          .build());

        items.add(factory
                .startActivity(BeaconsSettingsActivity.class)
                .labelText(R.string.main_navigation_beacons)
                .labelIcon(R.drawable.ic_beacons)
                .keepSingleInstance()
                .requireAuthentication()
                .setScope(SESSION_SCOPE)
                .build());
    }

    private final ScopedNavigationActionProvider scopedNavigationActionProvider = new ScopedNavigationActionProvider(items);

    public NavigationList(SessionProvider sessionProvider) {
        if (sessionProvider.getSession() == null) {
            setNullSessionScope();
        } else {
            setSessionScope();
        }
    }

    @Override
    public List<NavigationAction> getActions() {
        return scopedNavigationActionProvider.getActions();
    }

    public void setSessionScope() {
        scopedNavigationActionProvider.setScope(UNSCOPED | SESSION_SCOPE);
    }

    public void setNullSessionScope() {
        scopedNavigationActionProvider.setScope(UNSCOPED | NULL_SESSION_SCOPE);
    }
}
