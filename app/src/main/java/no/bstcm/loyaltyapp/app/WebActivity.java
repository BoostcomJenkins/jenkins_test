package no.bstcm.loyaltyapp.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;

import javax.inject.Inject;

import no.bstcm.loyaltyapp.app.dagger.components.DaggerMainNavigationActivityComponent;
import no.bstcm.loyaltyapp.app.dagger.components.MainNavigationActivityComponent;
import no.bstcm.loyaltyapp.app.dagger.modules.ActivityModule;
import no.bstcm.loyaltyapp.components.web.WebViewActivity;

public class WebActivity extends WebViewActivity {

    private static final String URL = "http://oslo-s.no/praktisk-info/kjope-billett/";

    @Inject
    Analytics analytics;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        injectDependencies();
        super.onCreate(savedInstanceState);
        analytics.trackWebsiteOpen();
    }

    @Override
    protected void onDestroy() {
        analytics.flush();
        super.onDestroy();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        analytics.trackWebsiteOpen();
    }

    @NonNull
    @Override
    public String getUrl() {
        return URL;
    }

    @NonNull
    @Override
    public String getTrackableName() {
        return "hpa";
    }

    private void injectDependencies() {
        MainNavigationActivityComponent component = DaggerMainNavigationActivityComponent
                .builder()
                .applicationComponent(((App) getApplication()).getComponent())
                .activityModule(new ActivityModule(this))
                .build();
        component.inject(this);
    }
}
