package no.bstcm.loyaltyapp.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import no.bstcm.loyaltyapp.app.dagger.components.ActivityComponent;
import no.bstcm.loyaltyapp.app.dagger.components.DaggerActivityComponent;
import no.bstcm.loyaltyapp.app.dagger.modules.ActivityModule;
import no.bstcm.loyaltyapp.components.beacons.Beacons;
import no.bstcm.loyaltyapp.components.beacons.BeaconsOnboardingActivity;
import no.bstcm.loyaltyapp.components.coupons.categories.CouponCategoriesActivity;
import no.bstcm.loyaltyapp.components.identity.PostAuthenticationOperation;
import no.bstcm.loyaltyapp.components.identity.core.SessionProvider;
import no.bstcm.loyaltyapp.components.identity.login.LoginActivity;
import no.bstcm.loyaltyapp.components.welcome.WelcomeActivity;

public class LauncherActivity extends AppCompatActivity {
    @Inject
    Analytics analytics;
    @Inject
    SessionProvider sessionProvider;
    @Inject
    PostAuthenticationOperation postAuthenticationOperation;

    private ActivityComponent component;
    private boolean requestActivityLaunch = false;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectDependencies();

        if (savedInstanceState == null) {
            requestActivityLaunch = true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        analytics.trackAppOpen();

        if (requestActivityLaunch) {
            launchNextActivity();
            requestActivityLaunch = false;
        }
    }

    private void launchNextActivity() {
        if (WelcomeActivity.shouldDisplay(this)) {
            startWelcomeActivity();
        } else if (Beacons.shouldDisplayOnboarding() && sessionProvider.getSession() != null) {
            startBeaconsOnboardingActivity();
        } else if (sessionProvider.getSession() != null) {
            postAuthenticationOperation.execute(sessionProvider.getSession());
            startMainActivityAndFinish();
        } else {
            startMainActivityAndFinish();
        }
    }

    private void startBeaconsOnboardingActivity() {
        startActivityForResult(BeaconsOnboardingActivity.getCallingIntent(this, null), BeaconsOnboardingActivity.REQUEST_WALKTHROUGH);
    }

    private void startMainActivityAndFinish() {
        startActivity(new Intent(getApplicationContext(), CouponCategoriesActivity.class));
        finish();
    }

    private void startLoginActivity() {
        startActivityForResult(LoginActivity.getCallingIntent(this, null), LoginActivity.REQUEST_AUTHENTICATION);
    }

    private void startWelcomeActivity() {
        startActivityForResult(new Intent(getApplicationContext(), WelcomeActivity.class), WelcomeActivity.REQUEST_WALKTHROUGH);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case LoginActivity.REQUEST_AUTHENTICATION:
                onAuthenticationResult(resultCode, data);
                break;
            case WelcomeActivity.REQUEST_WALKTHROUGH:
                onWelcomeResult(resultCode, data);
                break;
            case BeaconsOnboardingActivity.REQUEST_WALKTHROUGH:
                onBeaconsOnboardingResult(resultCode, data);
                break;
        }
    }

    private void onAuthenticationResult(int resultCode, Intent data) {
        switch (resultCode) {
            case LoginActivity.RESULT_SIGNED_IN:
            case LoginActivity.RESULT_SIGNED_UP:
            case LoginActivity.RESULT_CANCELED:
                startMainActivityAndFinish();
                break;
        }
    }

    private void onWelcomeResult(int resultCode, Intent data) {
        switch (resultCode) {
            case WelcomeActivity.RESULT_OK:
                startLoginActivity();
                break;
            case WelcomeActivity.RESULT_CANCELED:
                finish();
                break;
        }
    }

    private void onBeaconsOnboardingResult(int resultCode, Intent data) {
        startMainActivityAndFinish();
    }

    public void onDestroy() {
        analytics.flush();
        super.onDestroy();
    }

    private void injectDependencies() {
        component = DaggerActivityComponent
                .builder()
                .applicationComponent(((App) getApplication()).getComponent())
                .activityModule(new ActivityModule(this))
                .build();

        component.inject(this);
    }
}
