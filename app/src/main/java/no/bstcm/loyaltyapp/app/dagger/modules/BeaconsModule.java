package no.bstcm.loyaltyapp.app.dagger.modules;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import no.bstcm.loyaltyapp.app.Config;
import no.bstcm.loyaltyapp.components.analytics.dmp.BeaconsAnalyticsDmp;
import no.bstcm.loyaltyapp.components.common.ui.navigation.MainNavigationActivityDelegateFactory;
import no.bstcm.loyaltyapp.components.dmp.tracker.Tracker;
import okhttp3.OkHttpClient;

@Module
public class BeaconsModule {

    @Provides
    @Singleton
    no.bstcm.loyaltyapp.components.beacons.Config provideConfig(Tracker tracker,
                                                                MainNavigationActivityDelegateFactory mainNavigationActivityDelegateFactory,
                                                                OkHttpClient okHttpClient) {
        return no.bstcm.loyaltyapp.components.beacons.Config
                .builder()
                .setMainNavigationActivityDelegateFactory(mainNavigationActivityDelegateFactory)
                .setApiBaseUrl(Config.API_BEACONS_BASE_URL)
                .setApiKey(Config.API_BEACONS_KEY)
                .setOkHttpClient(okHttpClient)
                .setAnalytics(new BeaconsAnalyticsDmp(tracker))
                .setForegroundScanPeriod(5500, TimeUnit.MILLISECONDS)
                .setForegroundBetweenScanPeriod(30, TimeUnit.SECONDS)
                .setBackgroundScanPeriod(30, TimeUnit.SECONDS)
                .setBackgroundBetweenScanPeriod(2, TimeUnit.MINUTES)
                .build();
    }
}
