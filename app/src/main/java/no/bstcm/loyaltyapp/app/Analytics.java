package no.bstcm.loyaltyapp.app;

import no.bstcm.loyaltyapp.components.analytics.core.ApplicationAnalytics;
import no.bstcm.loyaltyapp.components.dmp.tracker.Tracker;

public class Analytics implements ApplicationAnalytics {

    private final Tracker tracker;

    public Analytics(Tracker tracker) {
        this.tracker = tracker;
    }

    @Override
    public void trackAppOpen() {
        tracker.track("app:open");
    }

    public void trackWebsiteOpen() {
        tracker.track("customer_website:open");
    }

    @Override
    public void flush() {
        tracker.flush();
    }
}
