package no.bstcm.loyaltyapp.app.dagger.modules;

import android.content.Context;

import java.util.Arrays;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import no.bstcm.loyaltyapp.app.Config;
import no.bstcm.loyaltyapp.app.NavigationList;
import no.bstcm.loyaltyapp.app.PostAuthenticationOperationImpl;
import no.bstcm.loyaltyapp.app.PostLogoutOperationImpl;
import no.bstcm.loyaltyapp.app.dagger.qualifiers.GuestState;
import no.bstcm.loyaltyapp.components.analytics.dmp.IdentityAnalyticsDmp;
import no.bstcm.loyaltyapp.components.beacons.Beacons;
import no.bstcm.loyaltyapp.components.common.ui.navigation.MainNavigationActivityDelegateFactory;
import no.bstcm.loyaltyapp.components.dmp.tracker.Tracker;
import no.bstcm.loyaltyapp.components.identity.Authenticator;
import no.bstcm.loyaltyapp.components.identity.InstantLoginNavigatorFactory;
import no.bstcm.loyaltyapp.components.identity.LoginConfirmationDialogNavigatorFactory;
import no.bstcm.loyaltyapp.components.identity.PostAuthenticationOperation;
import no.bstcm.loyaltyapp.components.identity.PostLogoutOperation;
import no.bstcm.loyaltyapp.components.identity.ProfileFormBuilder;
import no.bstcm.loyaltyapp.components.identity.core.AuthenticationNavigatorFactory;
import no.bstcm.loyaltyapp.components.identity.core.SessionProvider;
import no.bstcm.loyaltyapp.components.identity.msisdn.MsisdnFormat;
import no.bstcm.loyaltyapp.components.identity.msisdn.MsisdnParser;
import no.bstcm.loyaltyapp.components.identity.msisdn.MsisdnParserImpl;
import no.bstcm.loyaltyapp.components.identity.pickers.PredefinedFormFields;
import okhttp3.OkHttpClient;

@Module
public class IdentityModule {
    @Provides
    @Singleton
    SessionProvider provideSessionProvider(Context context, MsisdnParser parser) {
        return Authenticator.getInstance(context, parser);
    }

    @Provides
    @Singleton
    MsisdnParser providesMsisdnParser() {
        return MsisdnParserImpl
                .builder()
                .addFormat(MsisdnFormat.NORWAY)
                .build();
    }

    @Provides
    @Singleton
    AuthenticationNavigatorFactory provideAuthenticationNavigatorFactory() {
        return new LoginConfirmationDialogNavigatorFactory();
    }

    @Provides
    @GuestState
    @Singleton
    AuthenticationNavigatorFactory provideGuestStateAuthenticationNavigatorFactory() {
        return new InstantLoginNavigatorFactory();
    }

    @Provides
    @Singleton
    no.bstcm.loyaltyapp.components.identity.Config provideConfig(Context context,
                                                                 Tracker tracker,
                                                                 PostAuthenticationOperation postAuthenticationOperation,
                                                                 PostLogoutOperation postLogoutOperation,
                                                                 MainNavigationActivityDelegateFactory navigationDelegateFactory,
                                                                 @GuestState AuthenticationNavigatorFactory
                                                                         guestStateAuthenticationNavigatorFactory,
                                                                 MsisdnParser msisdnParser,
                                                                 OkHttpClient okHttpClient) {
        return no.bstcm.loyaltyapp.components.identity.Config
                .builder()
                .setApiBaseUrl(Config.API_USER_BASE_URL)
                .setApiLoyaltyClub(Config.API_USER_LOYALTY_CLUB)
                .setApiPublicToken(Config.API_USER_CUSTOMER_PUBLIC_TOKEN)
                .setAnalytics(new IdentityAnalyticsDmp(tracker))
                .setMainNavigationActivityDelegateFactory(navigationDelegateFactory)
                .setPostAuthenticationOperation(postAuthenticationOperation)
                .setGuestStateAuthenticationNavigatorFactory(guestStateAuthenticationNavigatorFactory)
                .setPostLogoutOperation(postLogoutOperation)
                .setRegistrationForm(getCommonProfileFormBuilder(context)
                                             .addRequiredField(PredefinedFormFields.password())
                                             .addRequiredField(PredefinedFormFields.termsAndConditions())
                                             .build())
                .setProfileForm(getCommonProfileFormBuilder(context).build())
                .setInterests(Arrays.asList(new String[]{}))
                .setLoginScreenFooterEnabled(false)
                .setMsisdnParser(msisdnParser)
                .setOkHttpClient(okHttpClient)
                .setPostAuthenticationIntentInterceptor(intent -> Beacons
                        .getOnboardingIntentInterceptor()
                        .interceptIntent(intent))
                .setCountryCodePickerEnabled(true)
                .build();
    }

    @Provides
    @Singleton
    PostAuthenticationOperation providePostAuthenticationOperation(NavigationList navigationList) {
        return new PostAuthenticationOperationImpl(navigationList);
    }

    @Provides
    @Singleton
    PostLogoutOperation providePostLogoutOperation(NavigationList navigationList) {
        return new PostLogoutOperationImpl(navigationList);
    }

    private ProfileFormBuilder getCommonProfileFormBuilder(Context context) {
        return new ProfileFormBuilder(context)
                .addRequiredField(PredefinedFormFields.firstName())
                .addRequiredField(PredefinedFormFields.lastName())
                .addRequiredField(PredefinedFormFields.birthday())
                .addOptionalField(PredefinedFormFields.gender())
                .addOptionalField(PredefinedFormFields.zipCode())
                .addOptionalField(PredefinedFormFields.email());
    }
}
