package no.bstcm.loyaltyapp.app.dagger.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import no.bstcm.loyaltyapp.components.app_linking.core.AppLinkHandler;
import no.bstcm.loyaltyapp.components.dmp.tracker.Tracker;
import no.bstcm.loyaltyapp.components.pusher.Config;

@Module
public class PusherModule {

    @Provides
    @Singleton
    Config providePusherConfig(AppLinkHandler appLinkHandler, Tracker dmpTracker) {
        return Config.builder()
                .setTracker(dmpTracker)
                .setHandler(appLinkHandler)
                .build();
    }

}
