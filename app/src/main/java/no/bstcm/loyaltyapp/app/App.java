package no.bstcm.loyaltyapp.app;

import android.app.Application;
import android.content.res.Configuration;
import android.support.annotation.NonNull;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import no.bstcm.loyaltyapp.app.dagger.HasComponent;
import no.bstcm.loyaltyapp.app.dagger.components.ApplicationComponent;
import no.bstcm.loyaltyapp.components.app_linking.AppLinkingApplication;
import no.bstcm.loyaltyapp.components.articles.ArticlesApplication;
import no.bstcm.loyaltyapp.components.beacons.Beacons;
import no.bstcm.loyaltyapp.components.beacons.BeaconsApplication;
import no.bstcm.loyaltyapp.components.beacons.Config;
import no.bstcm.loyaltyapp.components.common.ui.LocaleUtils;
import no.bstcm.loyaltyapp.components.coupons.CouponsApplication;
import no.bstcm.loyaltyapp.components.identity.IdentityApplication;
import no.bstcm.loyaltyapp.components.identity.core.UserSession;
import no.bstcm.loyaltyapp.components.opening_hours.OpeningHoursApplication;
import no.bstcm.loyaltyapp.components.pusher.Pusher;
import no.bstcm.loyaltyapp.components.pusher.PusherApplication;
import no.bstcm.loyaltyapp.components.shops.ShopsApplication;
import no.bstcm.loyaltyapp.components.web.WebApplication;
import no.bstcm.loyaltyapp.components.welcome.WelcomeApplication;
import no.bstcm.loyaltyapp.oslo_s.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class App extends Application implements HasComponent<ApplicationComponent>,
                                                ArticlesApplication,
                                                CouponsApplication,
                                                IdentityApplication,
                                                ShopsApplication,
                                                WelcomeApplication,
                                                PusherApplication,
                                                AppLinkingApplication,
                                                WebApplication,
                                                BeaconsApplication {

    private ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        LocaleUtils.forceAppLocale(this,LocaleUtils.Locales.NORWEGIAN);

        ApplicationInitializer.init(this);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                                              .setDefaultFontPath(getString(R.string.font_path))
                                              .setFontAttrId(R.attr.fontPath)
                                              .build());

        Pusher.initialize(this);
        Beacons.initialize(this);

        handlePostAuthenticationOperation();

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        Picasso.setSingletonInstance(new Picasso.Builder(this).downloader(new OkHttp3Downloader(getComponent().getOkHttpClient())).build());
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleUtils.forceAppLocale(this,LocaleUtils.Locales.NORWEGIAN);
    }

    private void handlePostAuthenticationOperation() {
        UserSession session = component
                .getSessionProvider()
                .getSession();
        if (session != null) {
            component
                    .getPostAuthenticationOperation()
                    .execute(session);
        }
    }

    @Override
    public ApplicationComponent getComponent() {
        return component;
    }

    public void setComponent(ApplicationComponent component) {
        if (this.component != null) {
            throw new RuntimeException("application component already initialized!");
        }

        this.component = component;
    }

    @Override
    public no.bstcm.loyaltyapp.components.articles.Config getArticlesConfig() {
        return component.getArticlesConfig();
    }

    @Override
    public no.bstcm.loyaltyapp.components.coupons.Config getCouponsConfig() {
        return component.getCouponsConfig();
    }

    @Override
    public no.bstcm.loyaltyapp.components.identity.Config getIdentityConfig() {
        return component.getIdentityConfig();
    }

    @Override
    public no.bstcm.loyaltyapp.components.shops.Config getShopsConfig() {
        return component.getShopsConfig();
    }

    @Override
    public no.bstcm.loyaltyapp.components.welcome.Config getWelcomeConfig() {
        return component.getWelcomeConfig();
    }

    @Override
    public no.bstcm.loyaltyapp.components.pusher.Config getPusherConfig() {
        return component.getPusherConfig();
    }

    @NonNull
    @Override
    public no.bstcm.loyaltyapp.components.app_linking.Config getAppLinkingConfig() {
        return component.getAppLinkingConfig();
    }

    @Override
    public no.bstcm.loyaltyapp.components.web.Config getWebConfig() {
        return component.getWebConfig();
    }

    @Override
    public Config getBeaconsConfig() {
        return component.getBeaconsConfig();
    }
}
