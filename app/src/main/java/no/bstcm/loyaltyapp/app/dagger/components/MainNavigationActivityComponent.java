package no.bstcm.loyaltyapp.app.dagger.components;

import dagger.Component;
import no.bstcm.loyaltyapp.app.OpeningHoursWebViewActivity;
import no.bstcm.loyaltyapp.app.WebActivity;
import no.bstcm.loyaltyapp.app.dagger.modules.ActivityModule;
import no.bstcm.loyaltyapp.app.dagger.modules.MainNavigationModule;
import no.bstcm.loyaltyapp.app.dagger.scopes.ActivityScope;
import no.bstcm.loyaltyapp.components.common.ui.navigation.MainNavigationActivityDelegate;
import no.bstcm.loyaltyapp.components.web.WebViewActivity;

@ActivityScope
@Component(modules = {
        ActivityModule.class,
        MainNavigationModule.class
}, dependencies = {ApplicationComponent.class})
public interface MainNavigationActivityComponent {
    void inject(WebActivity activity);
    void inject(OpeningHoursWebViewActivity activity);

    MainNavigationActivityDelegate getActivityDelegate();
}
