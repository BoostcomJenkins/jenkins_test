package no.bstcm.loyaltyapp.app.dagger.modules;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;

import dagger.Module;
import dagger.Provides;
import no.bstcm.loyaltyapp.app.NavigationList;
import no.bstcm.loyaltyapp.app.dagger.scopes.ActivityScope;
import no.bstcm.loyaltyapp.components.common.ui.navigation.ActivityClassActionSelector;
import no.bstcm.loyaltyapp.components.common.ui.navigation.MainNavigation;
import no.bstcm.loyaltyapp.components.common.ui.navigation.MainNavigationActivityDelegate;
import no.bstcm.loyaltyapp.components.identity.MenuAuthenticationExecutor;
import no.bstcm.loyaltyapp.components.identity.core.AuthenticationNavigator;
import no.bstcm.loyaltyapp.components.identity.core.AuthenticationNavigatorFactory;
import no.bstcm.loyaltyapp.components.identity.core.SessionProvider;

@Module
public class MainNavigationModule {
    @Provides
    @ActivityScope
    MainNavigationActivityDelegate provideMainNavigationActivityDelegate(AppCompatActivity activity, MainNavigation navigation) {
        return new MainNavigationActivityDelegate.Builder(activity, navigation).setItemIconTintSuppressed(false) // to use
                                                                               // @color/main_navigation_{icon,text}
                                                                               .build();
    }

    @Provides
    @ActivityScope
    MainNavigation provideMainNavigation(MainNavigation.ActionListProvider actionListProvider,
                                         MainNavigation.ActionExecutor actionExecutor,
                                         MainNavigation.ActionSelector actionSelector) {
        return new MainNavigation(actionListProvider, actionExecutor, actionSelector);
    }

    @Provides
    @ActivityScope
    MainNavigation.ActionSelector provideActionSelector(Activity activity) {
        return new ActivityClassActionSelector(activity.getClass());
    }

    @Provides
    @ActivityScope
    MainNavigation.ActionExecutor provideActionExecutor(Activity activity,
                                                        SessionProvider sessionProvider,
                                                        AuthenticationNavigator authenticationNavigator) {
        return new MenuAuthenticationExecutor(activity, sessionProvider, authenticationNavigator);
    }

    @Provides
    @ActivityScope
    AuthenticationNavigator provideAuthenticationNavigator(Activity activity, AuthenticationNavigatorFactory factory) {
        return factory.getNavigator(activity);
    }

    @Provides
    @ActivityScope
    MainNavigation.ActionListProvider provideActionListProvider(NavigationList navigationList) {
        return navigationList;
    }
}
