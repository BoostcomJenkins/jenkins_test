package no.bstcm.loyaltyapp.app.dagger.modules;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;

import dagger.Module;
import dagger.Provides;
import no.bstcm.loyaltyapp.app.dagger.scopes.ActivityScope;

@Module
public class ActivityModule {
    private final AppCompatActivity activity;

    public ActivityModule(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    Activity provideActivity() {
        return activity;
    }

    @Provides
    @ActivityScope
    AppCompatActivity provideAppCompatActivity() {
        return activity;
    }
}
