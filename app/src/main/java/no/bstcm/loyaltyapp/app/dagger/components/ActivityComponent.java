package no.bstcm.loyaltyapp.app.dagger.components;

import android.app.Activity;

import dagger.Component;
import no.bstcm.loyaltyapp.app.LauncherActivity;
import no.bstcm.loyaltyapp.app.dagger.modules.ActivityModule;
import no.bstcm.loyaltyapp.app.dagger.scopes.ActivityScope;

@ActivityScope
@Component(modules = {ActivityModule.class}, dependencies = {ApplicationComponent.class})
public interface ActivityComponent {
    void inject(LauncherActivity activity);

    Activity getActivity();
}