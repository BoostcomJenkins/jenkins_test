package no.bstcm.loyaltyapp.app.dagger.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import no.bstcm.loyaltyapp.app.Config;
import no.bstcm.loyaltyapp.app.dagger.qualifiers.GuestState;
import no.bstcm.loyaltyapp.components.analytics.dmp.ArticlesAnalyticsDmp;
import no.bstcm.loyaltyapp.components.common.ui.navigation.MainNavigationActivityDelegateFactory;
import no.bstcm.loyaltyapp.components.dmp.tracker.Tracker;
import no.bstcm.loyaltyapp.components.identity.core.AuthenticationNavigatorFactory;
import no.bstcm.loyaltyapp.components.identity.core.SessionProvider;
import okhttp3.OkHttpClient;

@Module
public class ArticlesModule {
    @Provides
    @Singleton
    no.bstcm.loyaltyapp.components.articles.Config provideConfig(Tracker tracker,
                                                                 SessionProvider sessionProvider,
                                                                 MainNavigationActivityDelegateFactory navigationDelegateFactory,
                                                                 AuthenticationNavigatorFactory authenticationNavigatorFactory,
                                                                 @GuestState AuthenticationNavigatorFactory
                                                                         guestStateAuthenticationNavigatorFactory,
                                                                 OkHttpClient okHttpClient) {
        return no.bstcm.loyaltyapp.components.articles.Config.builder()
                .setDataProvider(no.bstcm.loyaltyapp.components.articles.Config.DataProvider.ScraperApi)
                .setApiBaseUrl(Config.API_ARTICLES_BASE_URL)
                .setApiKey(Config.API_ARTICLES_KEY)
                .setAnalytics(new ArticlesAnalyticsDmp(tracker))
                .setSessionProvider(sessionProvider)
                .setMainNavigationActivityDelegateFactory(navigationDelegateFactory)
                .setAuthenticationNavigatorFactory(authenticationNavigatorFactory)
                .setGuestStateAuthenticationNavigatorFactory(guestStateAuthenticationNavigatorFactory)
                .setOkHttpClient(okHttpClient)
                .build();
    }
}
