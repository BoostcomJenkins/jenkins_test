package no.bstcm.loyaltyapp.app.dagger.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import no.bstcm.loyaltyapp.components.dmp.tracker.Config;
import okhttp3.OkHttpClient;

@Module
public class DmpModule {

    @Provides
    @Singleton
    Config provideDmpConfig(OkHttpClient okHttpClient) {
        return Config.builder()
                     .apiBaseUrl(no.bstcm.loyaltyapp.app.Config.API_DMP_BASE_URL)
                     .apiKey(no.bstcm.loyaltyapp.app.Config.API_DMP_KEY)
                     .okHttpClient(okHttpClient)
                     .build();
    }

}
