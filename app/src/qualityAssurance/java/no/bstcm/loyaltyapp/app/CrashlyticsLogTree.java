package no.bstcm.loyaltyapp.app;

import android.util.Log;

import com.crashlytics.android.Crashlytics;

import timber.log.Timber;

public class CrashlyticsLogTree extends Timber.Tree {
    @Override
    protected void log(int priority, String tag, String message, Throwable t) {
        if (priority != Log.VERBOSE && priority != Log.DEBUG) {
            Crashlytics.log(priority, tag, message);
        }
    }
}
